// LOEK SCRIPT JOHN
"use strict";

console.log('Yes i am alive');

// GLOBAL SCOPE
let itemsMenu = document.getElementById('itemsMenu');
let inhoud = document.getElementById('inhoud');
let footer = document.getElementById('footer');

// MENU-CONTROLLER
let menuController = (function() {

    let backColour = document.getElementsByTagName('body')[0];
    let hamburgerMenu = document.getElementById('hamburgerMenu');
    let kruisMenu = document.getElementById('kruisMenu');
    let uitklapMenu = document.getElementById('uitklapMenu');

    hamburgerMenu.addEventListener('click', function() {
        hamburgerMenu.style.display = 'none';
        kruisMenu.style.display = 'block';
        uitklapMenu.style.display = 'flex';
        backColour.style.backgroundColor = '#ebab11';
    });

    kruisMenu.addEventListener('click', function() {
        hamburgerMenu.style.display = 'block';
        kruisMenu.style.display = 'none';
        uitklapMenu.style.display = 'none';
        backColour.style.backgroundColor = '#000';
    });

})();

// UI-BACKGROUND-COLOUR-CONTROLLER
let backController = (function() {

    let backColour = document.getElementsByTagName('body')[0];
    let menuLinks = itemsMenu.getElementsByTagName('p');

    let johnKleuren = ['green','red','blue','yellow','#444','grey'];

    for ( var i=0; i<johnKleuren.length; i++ ) {
        function klikMenu(i) {
            menuLinks[i].addEventListener('mouseover', function() {
            backColour.style.backgroundColor = johnKleuren[i];
        });    
    }
        klikMenu(i);
    }

})();